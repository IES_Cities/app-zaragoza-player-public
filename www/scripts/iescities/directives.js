/* Directives */
var iescitiesAppDirectives = angular.module('iescitiesAppDirectives', []);

/** Main menu directive */
iescitiesAppDirectives
		.directive(
				"menuPanel",				
				function() {
					return function($scope) {

						var addedPanels = 0;
						
						// Close panel
						$scope.closePanel = function(panelId) {
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
							}
							document.getElementById(panelId).style.zIndex = 999;
						};

						// Closes all panels
						$scope.closeAllPanels = function() {
							$scope.closePanel('appsList');
							$scope.closePanel('appSearch');
							$scope.closePanel('myApps');
							$scope.popPanel('appDetails');
						};

						// Close all panels and unfade menu
						$scope.fade_back = function() {							
							if (isWebKit()) {
								document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById('main_menu').style.left = '-14em';
							}
							document.getElementById('black_fader').style.zIndex = '0';
							document.getElementById('black_fader').style.opacity = '0';
						};

						// Toggles main menu
						$scope.toggle_main_menu = function() {
							if (document.getElementById('black_fader').style.zIndex == ""
									|| document.getElementById('black_fader').style.zIndex == "0") {

								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(14em,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '0';
								}
								document.getElementById('black_fader').style.zIndex = '990';
								document.getElementById('black_fader').style.opacity = '0.5';

							} else {
								// Ocultar menu
								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '-14em';
								}
								document.getElementById('black_fader').style.zIndex = '0';
								document.getElementById('black_fader').style.opacity = '0';

								document.getElementById('edit_map').style.zIndex = '1490';
							}
						};

						$scope.showPanel = function(panelId) {
							console.log("menuPanel.showPanel - " + panelId);
							$scope.fade_back();
							$scope.closeAllPanels();
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
							hideKeyboard();
						};

						$scope.addPanel = function(panelId) {							
							console.log("menuPanel.addPanel - " + panelId + addedPanels);
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;							
						};

						$scope.popPanel = function(panelId) {							
							console.log("menuPanel.popPanel - " + panelId);
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
							}
							document.getElementById(panelId).style.zIndex = 999;
						};
						
	

					};
				});


/** Apps list directive */
iescitiesAppDirectives.directive("appsList", function() {
	return function($scope) {
		//start survey
		startRatingSurvey();
		//init logging
		Restlogging.init("https://iescities.com");

	};
});

/** App search directive */
iescitiesAppDirectives.directive("appSearch", function() {
	return function($scope) {

	};
});


/** Profile panel directive */
iescitiesAppDirectives.directive("profilePanel", function() {
	return function($scope) {

	};
});

/** New user panel directive */
iescitiesAppDirectives.directive("newUserPanel", function() {
	return function($scope) {

	};
});

/** Login panel directive */
iescitiesAppDirectives.directive("loginPanel", function() {
	return function($scope) {

	};
});

/** Help info panel directive */
iescitiesAppDirectives.directive("helpInfoPanel", function() {
	return function($scope) {

	};
});


/** Write comment directive */
iescitiesAppDirectives.directive("writeCommentPanel", function() {
	return function($scope) {

	};
});



/**  check if src exists */
iescitiesAppDirectives.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
    }
});