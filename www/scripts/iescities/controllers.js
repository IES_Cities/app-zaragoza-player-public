'use strict';

/* Controllers */
var iescitiesAppControllers = angular.module('iescitiesAppControllers', []);

/** Main controller */
// FIXME: Sacar controlador de main.html y crear los necesarios
iescitiesAppControllers
		.controller(
				'MainCtrl',
				[
						'$rootScope',
						'$scope',
						'geolocation',
						function($rootScope, $scope, geolocation) {
							$scope.gPlace;
							$scope.Math = window.Math;

							// Geolocation button
							$scope.geolocateMe = function() {
								document.getElementById("my_location_btn").className = "my_location_btn_spinner ng-scope";
								document.getElementById("my_location_btn").disabled = true;

								geolocation
										.getCurrentPosition(
												function(position) {
													console
															.log("MainCtrl.getCurrentPosition() - "
																	+ JSON
																			.stringify(position));
													$scope
															.centerMapInPosition(position);
												},
												function(error) {
													toastr
															.error('No ha sido posible geoposicionar su ubicaci��n.');
													document
															.getElementById("my_location_btn").className = "my_location_btn_normal ng-scope";
													document
															.getElementById("my_location_btn").disabled = false;
												}, {
													maximumAge : 3000,
													timeout : 10000,
													enableHighAccuracy : false
												});
							};

						} ]);

/** Menu controller */
iescitiesAppControllers.controller('menuCtrl', [ '$rootScope', '$scope', 'ApiService',
		function($rootScope, $scope, ApiService) {

			// Mostrar lista de aplicaciones
			$scope.openAppsList = function() {
				$scope.showPanel('appsList');
				$( "#accordion" ).accordion({
				      active: false
				    });
			};

			// Mostrar buscador de aplicaciones
			$scope.openAppSearch = function() {
				$scope.showPanel('appSearch');
				$( "#accordion" ).accordion({
				      collapsible: true,
				      active: false
				    });
				
				
				 
			};

			// Mostrar mis aplicaciones
			$scope.openMyApps = function() {
				$scope.showPanel('myApps');
				$( "#accordion" ).accordion({
				      active: false
				    });
			};

		} ]);

/** Profile controller */
iescitiesAppControllers
		.controller(
				'profileCtrl',
				[
						'$rootScope',
						'$scope',
						'$http',
						function($rootScope, $scope, $http) {

							// Close panel
							$scope.closePanel = function(panelId) {
								if (isWebKit()) {
									document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
								} else {
									document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
								}
								document.getElementById(panelId).style.zIndex = 999;
							};

							// Creates new user
							$scope.createNewUser = function() {

								if ($scope.profileData == null) {
									toastr
											.info("El correo y el password son obligatorios");
									return;
								}

								if ($scope.profileData.name == null) {
									toastr.info("El nombre es obligatorio");
									return;
								}

								if ($scope.profileData.email == null) {
									toastr
											.info("No has introducido el correo electr��nico");
									return;
								}

								if ($scope.profileData.password == null) {
									toastr
											.info("No has introducido el password");
									return;
								}

								if ($scope.profileData.passwordConfirm == null) {
									toastr
											.info("No has confirmado el password");
									return;
								}

								if ($scope.profileData.password != $scope.profileData.passwordConfirm) {
									toastr.info("El password no coincide");
									return;
								}

								var schoolType = false;
								if ($scope.profileData.school == false
										|| $scope.profileData.school == undefined) {
									schoolType = false;
								} else {
									schoolType = true;
								}

								var profileBody = {
									"name" : $scope.profileData.name,
									"email" : $scope.profileData.email,
									"password" : $scope.profileData.password,
									// nif opcional
									"nif" : $scope.profileData.nif
								};

							};

							// Updated current user profile
							$scope.updateProfile = function() {

								var profileBody = {
									"name" : $scope.profileData.name,
									"email" : $scope.profileData.email,
									"password" : $scope.profileData.password,
									// nif opcional
									"nif" : $scope.profileData.nif
								};

								if ($scope.profileData == null) {
									toastr
											.info("El correo y el password son obligatorios");
									return;
								}

								if ($scope.profileData.email == null) {
									toastr
											.info("No has introducido el correo electr��nico");
									return;
								}

								if ($scope.profileData.password == null) {
									toastr
											.info("No has introducido el password");
									return;
								}

								if ($scope.profileData.currentPassword != $scope.profileData.password) {
									toastr
											.info("El password actual no es v��lido");
									return;
								}

								if ($scope.profileData.newPassword != undefined
										&& $scope.profileData.newPassword.length > 0) {
									profileBody.password = $scope.profileData.newPassword;
									$scope.profileData.password = $scope.profileData.newPassword;
								}

							};

							$scope.closePanelWithId = function(panelId) {
								$scope.closePanel(panelId);
							};

						} ]);

/** Login controller */
iescitiesAppControllers
		.controller(
				'loginCtrl',
				function($scope, $rootScope, $http) {

					// Close panel
					$scope.closePanel = function(panelId) {
						if (isWebKit()) {
							document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
						} else {
							document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
						}
						document.getElementById(panelId).style.zIndex = 999;
					};

					$scope.rememberLoginData = window.localStorage
							.getItem("rememberLoginData") == 'true' ? true
							: false;

					// Load stored account credential values
					$scope.loginData = {
						email : window.localStorage.getItem("email"),
						password : window.localStorage.getItem("password")
					};

					$scope.userLogin = function() {
						if ($scope.loginData == null) {
							toastr.info("Email o contrase��a no v��lido");
							return;
						}
					};

					$scope.closePanelWithId = function(panelId) {
						$scope.closePanel(panelId);
					};

				});

/** userMapsCtrl */
iescitiesAppControllers
		.controller(
				'userMapsCtrl',
				[
						'$rootScope',
						'$scope',
						function($rootScope, $scope) {

							// Close panel
							$scope.closePanel = function(panelId) {
								if (isWebKit()) {
									document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
								} else {
									document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
								}
								document.getElementById(panelId).style.zIndex = 999;
							};

							$scope.translateMapType = function(mapType) {
								if (mapType == "public") {
									mapType = "P��blico"
								} else if (mapType == "private") {
									mapType = "Privado";
								} else {
									mapType = "Colaborativo";
								}
								return mapType;
							};

							$scope.showUserMap = function(map) {
								// FIXME: centrar en funcion de los marcadores,
								// layer.getbounds...
								$scope.centerMapInZaragoza();

								$rootScope.userMap = true;
								$scope.refreshMapData(map);
								$scope.closePanel('userMapsPanel');
							};

							$scope.showDeleteMapDialog = function(mapId) {
								$rootScope.mapIdToDelete = mapId;
								$rootScope.deleteMapDialogVisible = true;
							};

							$scope.closePanelWithId = function(panelId) {
								$scope.closePanel(panelId);
							};

						} ]);

/** helpCtrl */
iescitiesAppControllers.controller('helpCtrl', [ '$rootScope', '$scope',
		function($rootScope, $scope) {

			$scope.closeHelpPanel = function() {
				$rootScope.helpVisible = false;
			};

		} ]);

/** dialogCtrl */
iescitiesAppControllers
		.controller(
				'dialogCtrl',
				[
						'$rootScope',
						'$scope',
						function($rootScope, $scope) {

							// Close panel
							$scope.closePanel = function(panelId) {
								if (isWebKit()) {
									document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
								} else {
									document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
								}
								document.getElementById(panelId).style.zIndex = 999;
							};

							// Delete selected map
							$scope.acceptDialog = function() {
								$scope.closePanel('edit_map_desc');
							};

							$scope.cancelDialog = function() {
								$rootScope.deleteMapDialogVisible = false;
							};

						} ]);

/**
 * Controller - Privacy
 * 
 * Politica de privacidad
 * 
 */
iescitiesAppControllers.controller('privacyCtrl', [
		'$rootScope',
		'$scope',
		function($rootScope, $scope) {

			$rootScope.privacyVisible = false;

			var isPrivacyAccepted = window.localStorage
					.getItem("isPrivacyAccepted");

			if (isPrivacyAccepted) {
				$rootScope.privacyVisible = false;
			} else {
				$rootScope.privacyVisible = true;
			}

			$scope.acceptPrivacy = function() {
				$rootScope.privacyVisible = false;
				window.localStorage.setItem("isPrivacyAccepted", true);
			};

			$scope.refusePrivacy = function() {
				navigator.app.exitApp();
			};

		} ]);

/**
 * Controller - Apps List
 * 
 * Lista de aplicaciones disponibles
 * 
 */
iescitiesAppControllers.controller('appsListCtrl', [
		'$rootScope',
		'$scope',
		'$log',
		'SesionData',
		'ApiService',
		function($rootScope, $scope, $log, SesionData, ApiService) {
			console.log("¡¡¡inicio aplicacion!!!");
			$log.debug("appsListCtrl.init()");
			$rootScope.iesCitiesApps = null;
			$rootScope.iesRatings = null;
			if(localStorage.aplicaciones && localStorage.aplicaciones != "undefined" && localStorage.aplicaciones != ""){
				$log.debug("appsListCtrl.init() " + localStorage.aplicaciones);
				$rootScope.iesCitiesApps = JSON.parse(localStorage.aplicaciones);
			}

			ApiService.getAllRatings(function(data) {
				
					$log.debug("appsListCtrl.getAllRatings() - apps: "
							+ data.count);
					$rootScope.iesRatings = data.rows;
					$rootScope.$apply();
					ApiService.getIESCitiesApps(function(data) {
						
						$log.debug("Lista de aplicaciones disponibles getIESCitiesApps() - apps: "
								+ data);

						if (data != null && data.response != null
								&& data.response.docs != null) {
							
							$rootScope.iesCitiesApps = data.response.docs;
							$rootScope.$apply();

						}
						else {
							$log.debug("getIESCitiesApps error con el servicio");
							
						}
					});
				
			});
			
			
			
			
			$rootScope.$watch('iesCitiesApps', function() {
			       localStorage.setItem("aplicaciones", JSON.stringify($rootScope.iesCitiesApps));
			       $rootScope.$apply();
			   });

			
			


			$scope.obtenerCategoriaAplicacion = function(app) {
				var categorias = "";
				if (app != null && app.temas_smultiple != null
						&& app.temas_smultiple.length > 0) {
					var size = app.temas_smultiple.length;
					for (var i = 0; i < size; i++) {
						var space = i == 0 ? '' : ' - ';
						categorias = categorias + space
								+ app.temas_smultiple[i];
					}

				}
				return categorias;
			};

			$scope.obtenerPlataformasAplicacion = function(app) {
				var plataformas = "";
				if (app != null && app.plataforma_smultiple != null
						&& app.plataforma_smultiple.length > 0) {
					var size = app.plataforma_smultiple.length;
					for (var i = 0; i < size; i++) {
						var space = i == 0 ? '' : ' - ';
						plataformas = plataformas + space
								+ app.plataforma_smultiple[i];
					}

				}
				return plataformas;
			};

			$scope.mostrarDetalleAplicacion = function(app) {
				$log.debug("appsListCtrl.mostrarDetalleAplicacion()");
				$rootScope.selectedApp = app;
				$log.debug("appsListCtrl.mostrarDetalleAplicacion() 2"+app.id);
				ApiService.getAvgRating(app.id,function(data) {
					$log.debug("appsListCtrl.mostrarDetalleAplicacion() 3" +data.rows[0].avgrating);
					$rootScope.selectedAppRating = Math.round(data.rows[0].avgrating);
					$log.debug("appsListCtrl.mostrarDetalleAplicacion() 4" +$rootScope.selectedAppRating);
					$rootScope.$apply();
					
				});
				$scope.addPanel('appDetails');
			};
			
			$scope.existePara = function(plataforma,app) {
				var existe = false;
				if (app.os_smultiple != null && app.os_smultiple.indexOf(plataforma) > -1 ) {
					existe = true;
				}
				return existe;
			};
			
			$scope.getRating = function(appId) {
				var result = $.grep($rootScope.iesRatings, function(e){ return e.app == appId; });
				
				if (result.length == 0) {

					  return 0;
					} else {

					  return Math.round(result[0].avgrating);
					}
				
				return 0;
			};

		} ]);

/**
 * Controller - App Details
 * 
 * Detalle de la aplicacion seleccionada
 * 
 */
iescitiesAppControllers.controller('appsDetailsCtrl', [ '$rootScope', '$scope', '$log', '$window', 'ApiService', function($rootScope, $scope, $log, $window, ApiService) {

			$log.debug("appsDetailsCtrl.init()");
			
			// FACEBOOK
			console.log('check facebookInstalled');
			appAvailability.check("com.facebook.katana", // Package Name
					function() { // Success callback
				console.log('FACEBOOK AVAILABLE');
				$rootScope.facebookInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('FACEBOOK NOT available');
						$rootScope.facebookInstalled = false;
					}
			);
			
			// TWITTER
			console.log('check TWITTER');
			appAvailability.check("com.twitter.android", // Package Name
					function() { // Success callback
				console.log('TWITTER AVAILABLE');
				$rootScope.twitterInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('TWITTER NOT available');
						$rootScope.twitterInstalled = false;
					}
			);
			
			
			// WHATSAPP
			console.log('check WHATSAPP');
			appAvailability.check("com.whatsapp", // Package Name
					function() { // Success callback
				console.log('WHATSAPP AVAILABLE');
				$rootScope.whatsappInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('WHATSAPP NOT available');
						$rootScope.whatsappInstalled = false;
					}
			);
			
			
			$scope.verComentarios = function() {
				$rootScope.appCommentsVisible = true;
				$log.debug("appsDetailsCtrl.getComments()");
				ApiService.getComments($scope.selectedApp.id,function(data) {
					$log.debug("appsDetailsCtrl.getComments() - RESPUESTA:"+data.count);
					$rootScope.comentarios = data.count;	
					if (data.count > 0){
						$rootScope.platformComments = data.rows;
					} else {
						$rootScope.platformComments =[];
					}
					$scope.popPanel('appDetails');
					$scope.addPanel('appComments');		
					$rootScope.$apply();
					
				});

				
			};

			$scope.instalarAplicacion = function() {
				
				var packageName = "";
				
				
				if ($rootScope.selectedApp.android_t){
				if ($rootScope.selectedApp.android_t.indexOf("id=")>0){

					appAvailability.check($rootScope.selectedApp.android_t.substring($rootScope.selectedApp.android_t.indexOf("id=")+3), // Package Name
							function() { // Success callback
						console.log('App available');
						
						
						toastr
						.info('La aplicación ya se encuentra instalada.');

							}, function() { // Error callback
								console.log('App not available');
								Restlogging.appLog(logType.CONSUME,"app installed");
								console.log('CONSUME app installed');
								$window.open($rootScope.selectedApp.android_t,  '_blank', 'EnableViewPortScale=yes');
							}
					);
				}
				
				
				else {
					toastr
					.info('Esta aplicación no está disponible para Android.');
				}
				
				}
	
			};
			
			
			
			
//			$scope.facebookInstalled = function() {
//				console.log('check facebookInstalled');
//				appAvailability.check("com.facebook.katana", // Package Name
//						function() { // Success callback
//					console.log('Facebook AVAILABLE');
//					return true;
//						}, function() { // Error callback
//							console.log('Facebook NOT available');
//							return false;
//						}
//				);
//				console.log('Facebook Facebook');
//				return true;
//		};
//		
//		$scope.twitterInstalled = function() {
//			console.log('check twitter');
//			appAvailability.check("com.twitter.android", // Package Name
//					function() { // Success callback
//				console.log('Twitter AVAILABLE');
//				return true;
//					}, function() { // Error callback
//						console.log('Twitter NOT available');
//						return false;
//					}
//			);
//		
//	};
//	
//	$scope.whatsappInstalled = function() {
//		console.log('check whatsapp');
//		appAvailability.check("com.whatsapp", // Package Name
//				function() { // Success callback
//			console.log('Whatsapp AVAILABLE');
//			return true;
//				}, function() { // Error callback
//					console.log('Whatsapp NOT available');
//					return false;
//				}
//		);
//	
//};
			
			
			
			$scope.shareTwitter = function() {
				
				$window.plugins.socialsharing.shareViaTwitter($scope.selectedApp.title+". Descárgala en: "+$scope.selectedApp.android_t)
					.then(function(result) {
				      // Success!
						Restlogging.appLog(logType.COLLABORATE,"comment shared on social networks");
						toastr
						.info('Mensaje enviado correctamente!');
					}, function(err) {
			      // An error occurred. Show a message to the user
						toastr
						.error('Se ha producido un error');
			    });
								
		};
			
			

			
			$scope.shareFacebook = function() {
				
				$window.plugins.socialsharing.shareViaFacebook($scope.selectedApp.title+". Descárgala en: "+$scope.selectedApp.android_t)
				.then(function(result) {
			      // Success!
					Restlogging.appLog(logType.COLLABORATE,"comment shared on social networks");
					toastr
					.info('Mensaje enviado correctamente!');
				}, function(err) {
		      // An error occurred. Show a message to the user
					toastr
					.error('Se ha producido un error');
				});
			};
			
			
			$scope.shareWhatsapp = function() {
				
				$window.plugins.socialsharing.shareViaWhatsApp($scope.selectedApp.title+". Descárgala en: "+$scope.selectedApp.android_t)
					.then(function(result) {
				      // Success!
						Restlogging.appLog(logType.COLLABORATE,"comment shared on social networks");
						toastr
						.info('Mensaje enviado correctamente!');
					}, function(err) {
			      // An error occurred. Show a message to the user
						toastr
						.error('Se ha producido un error');
			    });
								
		};
				
			$scope.shareGlobal = function() {
					
					$window.plugins.socialsharing.share($scope.selectedApp.title+". Descárgala en: "+$scope.selectedApp.android_t)
					.then(function(result) {
				      // Success!
						Restlogging.appLog(logType.COLLABORATE,"comment shared on social networks");
						toastr
						.info('Mensaje enviado correctamente!');
					}, function(err) {
			      // An error occurred. Show a message to the user
						toastr
						.error('Se ha producido un error');
			    });
			};

			$scope.closeAppDetails = function() {
				$scope.popPanel('appDetails');
				$rootScope.selectedApp = null;
			};
			
			$scope.existePara = function(plataforma) {
				var existe = false;
				if ($rootScope.selectedApp != null && $rootScope.selectedApp.os_smultiple != null && $rootScope.selectedApp.os_smultiple.indexOf(plataforma) > -1 ) {
					existe = true;
				}
				return existe;
			};
			

		} ]);

/**
 * Controller - App Comments
 * 
 * Comentarios de la aplicacion seleccionada
 * 
 */
iescitiesAppControllers.controller('appCommentsCtrl', [ '$rootScope', '$scope',
		'$log', function($rootScope, $scope, $log) {

			$log.debug("appsCommentsCtrl.init()");
			
			$rootScope.comentarios = "2";
			$rootScope.platformComments = [];
	
			$scope.closeAppComments = function() {
				$scope.popPanel('appComments');
				$scope.addPanel('appDetails');
			};
			
			$scope.formatDate = function(dateStr) {
				var month = dateStr.substring(4,7);
				var day = dateStr.substring(8,10);
				var year = dateStr.substring(11,15);
				return day+"/"+month+"/"+year;
			};

			$scope.desescapar = function(texto) {

				var textoEscapado = texto.replace(/\\'/g,"\'");
				return textoEscapado;
			};
			
	}
]);

/**
 * Controller - App Search
 * 
 * Buscador de aplicaciones
 * 
 */
iescitiesAppControllers.controller('appSearchCtrl', ['$rootScope', '$scope', '$log', 'SesionData', 'ApiService', function($rootScope, $scope, $log, SesionData, ApiService) {
	$log.debug("appSearchCtrl.init()");
	$scope.byTitle = ""; //Es el valor por defecto
	$scope.selectedCategory = "Todas"; //Es el valor por defecto

//Array de CATEGORIAS FIJAS
	var categoriasFijas = ['Movilidad', 
						'Eventos', 
						'Equipamientos', 
						'Medio ambiente', 
						'Salud', 
						'Deporte',
						'Juventud',
						'Niñ@s',
						'Tecnología' ]; 

	$scope.categories = []; //Array de CATEGORIAS
	$scope.categories.push({'name':'Todas','selected':true, 'filtroTodas':true}); //Para que en el menú el usuario pueda elegir no filtrar por categorías
	
	angular.forEach(categoriasFijas, function(cat, key){
		$scope.categories.push({'name': cat ,'selected':false}); //Añadir categoría
	}); 


// RECORRE LAS APLICACIONES AÑADIENDO LAS CATEGORIAS ENCONTRADAS
/*		if ($rootScope.iesCitiesApps != null) { //Si hay aplicaciones
			$log.debug("Buscador de aplicaciones getIESCitiesApps() - apps: " + $rootScope.iesCitiesApps.length);

			var apps = $rootScope.iesCitiesApps; //Rendimiento
			angular.forEach(apps, function(app, index) { //Recorrer aplicaciones
				var cats = app.temas_smultiple; //Array de categorias de una aplicación
				angular.forEach(cats, function(category, index) { //Recorrer categorías de esa app
					var existe = false;
					angular.forEach($scope.categories, function(addedCategory, index) { //Si no existe ya en el array de TODAS las categorías
						if (addedCategory.name == category){
							existe = true;
							return;
						}
					});	
					if (!existe) {
						$scope.categories.push({'name': category ,'selected':false}); //Añadir categoría
						$log.debug("Category added:" + category);
					}					
				});
			});
	}*/




	$scope.obtenerCategoriaAplicacion = function(app) {
		var categorias = "";
		if (app != null && app.temas_smultiple != null && app.temas_smultiple.length > 0) {
			var size = app.temas_smultiple.length;
			for (var i = 0; i < size; i++) {
				var space = i == 0 ? '' : ' - ';
				categorias = categorias + space + app.temas_smultiple[i];
			}
		}
		return categorias;
	};

	$scope.obtenerPlataformasAplicacion = function(app) {
		var plataformas = "";
		if (app != null && app.plataforma_smultiple != null && app.plataforma_smultiple.length > 0) {
			var size = app.plataforma_smultiple.length;
			for (var i = 0; i < size; i++) {
				var space = i == 0 ? '' : ' - ';
				plataformas = plataformas + space + app.plataforma_smultiple[i];
			}
		}
		return plataformas;
	};


	$scope.mostrarDetalleAplicacion = function(app) {
		$log.debug("appSearchCtrl.mostrarDetalleAplicacion()");
		$rootScope.selectedApp = app;
		$scope.addPanel('appDetails');
	};

	$scope.correspondeConCategoria = function(app) {
		var coincide = false;
		if ($scope.categories[0].selected) {
			coincide = true;
		} else {
			angular.forEach(app.temas_smultiple, function(appCategory, index) {
				// Buscar si la categoría esta seleccionada
				angular.forEach($scope.categories, function(category, index) {
					if (category.name == appCategory && category.selected){
						coincide = true;
						return;
					}	
				});								
			});		
		}				
		return coincide;
	};
	
	
	$scope.selectCagetory = function(category) {	
		Restlogging.appLog(logType.CONSUME,"Category search");
		if (category.name != "Todas" && !category.selected) {
			$scope.categories[0].selected = false;
		}		
		if (category.name == "Todas" && !category.selected) {
			$scope.categories[0].selected = false;
			angular.forEach($scope.categories, function(category, index) {
				
				category.selected = false;
			});	
		}		
		category.selected = !category.selected;		
	};
	
	$scope.existePara = function(plataforma,app) {
		var existe = false;
		if (app.plataforma_smultiple != null && app.plataforma_smultiple.indexOf(plataforma) > -1 ) {
			existe = true;
		}
		return existe;
	};
	
	$scope.getRating = function(appId) {

		var result = $.grep($rootScope.iesRatings, function(e){ return e.app == appId; });
		
		if (result.length == 0) {

			  return 0;
			} else {

			  return Math.round(result[0].avgrating);
			}
		
		return 0;
	};
	
} ]);


/**
 * Controller - Installed Apps
 * 
 * Listado de aplicaciones instaladas
 * 
 */
iescitiesAppControllers.controller('myAppsCtrl', [
		'$rootScope',
		'$scope',
		'$log',
		'ApiService',

		function($rootScope, $scope, $log, ApiService) {

			$log.debug("myAppsCtrl.init()");

			$scope.installedApps = [];	


				if ( $rootScope.iesCitiesApps != null) {
					$log.debug("Listado de aplicaciones instaladas getIESCitiesApps() - apps: "
							+ $rootScope.iesCitiesApps.length);

					// Todo: Recorrer lista de aplicaciones y mirar cuales estan
					angular.forEach($rootScope.iesCitiesApps, function(app, index) {
						//TODO:En desarrollo
						//$scope.installedApps.push(app);
						console.log("app android_t = "+app.android_t);
						if(app.android_t){
						if (app.android_t.indexOf("id=")>0){
							$log.debug("Check if installed: "+ app.android_t.substring(app.android_t.indexOf("id=")+3));
							//TODO:En produccion
							appAvailability.check(app.android_t.substring(app.android_t.indexOf("id=")+3), // Package Name
									function() { // Success callback
										$scope.installedApps.push(app);
									}, function() { // Error callback
										console.log('App not available');
									}
							);
							
//							$scope.installedApps.push(app); // descomentar lo de arriba 
							
						}
						else {
							console.log('No hay parametro android_t');
						}
						}

					});

				}
//			});

			$scope.mostrarEscribirComentario = function(app) {
				$log.debug("myAppsCtrl.mostrarEscribirComentario()");
				$rootScope.selectedApp = app;
				$scope.addPanel('appRating');
			};

			// Ejemplo appAvailability plugin
//			appAvailability.check('com.twitter.android', // Package Name
//			function() { // Success callback
//				console.log('Twitter is available');
//			}, function() { // Error callback
//				console.log('Twitter is not available');
//			});
			
			$scope.existePara = function(plataforma,app) {
				var existe = false;
				if (app.os_smultiple != null && app.os_smultiple.indexOf(plataforma) > -1 ) {
					existe = true;
				}
				return existe;
			};
			
			$scope.getRating = function(appId) {

				var result = $.grep($rootScope.iesRatings, function(e){ return e.app == appId; });
				
				if (result.length == 0) {

					  return 0;
					} else {

					  return Math.round(result[0].avgrating);
					}
				
				return 0;
			};

		} ]);


/**
 * Controller - App Rating
 * 
 * Valorar aplicaciones
 * 
 */
iescitiesAppControllers.controller('appRatingCtrl', [ '$rootScope', '$scope', '$log','ApiService', function($rootScope, $scope, $log, ApiService) {

			$log.debug("appRatingCtrl.init()");
			
			$scope.appRatings = [ "1", "2", "3", "4", "5" ];
			
			$scope.commentRating = 0;

			$scope.closeAppRating = function() {
				$scope.popPanel('appRating');
				$scope.addPanel('myApps');
			};

			$scope.sendRatting = function() {
				ApiService.insertComment($scope.comment,$scope.review,$scope.selectedApp,$scope.commentRating, new Date(),function(data) {
		  			if (data != "error") {
		  				
		  				// actualizar 
		  				ApiService.getAllRatings(function(data) {
		  					
							$log.debug("appsListCtrl.getAllRatings() - apps: "
									+ data.count);
							$rootScope.iesRatings = data.rows;
							$rootScope.$apply();						
					});
		  				
		  				toastr
						.info('Su valoración ha sido enviada.');
		  				Restlogging.appLog(logType.PROSUME,"comment sent");	
						$scope.closeAppRating();
					} else {
						toastr
						.error('Se ha producido un error');
						$scope.closeAppRating();
					}
					
				});
			
			};
			
			
			
		} ]);