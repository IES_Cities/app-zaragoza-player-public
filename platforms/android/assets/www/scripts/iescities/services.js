'use strict';

/* Services */
var iescitiesAppServices = angular.module('iescitiesAppServices', []);

/** Cordova ready listener */
iescitiesAppServices.factory('cordovaReady', function() {
    return function(fn) {

        var queue = [];

        var impl = function() {
            queue.push(Array.prototype.slice.call(arguments));
        };

        document.addEventListener('deviceready', function() {
            queue.forEach(function(args) {
                fn.apply(this, args);
            });
            impl = fn;
        }, false);

        return function() {
            return impl.apply(this, arguments);
        };
    };
});

/** Geolocation service */
iescitiesAppServices.factory('geolocation', [
    '$rootScope',
    'cordovaReady',
    function($rootScope, cordovaReady) {
        return {
            getCurrentPosition: function(onSuccess, onError, options) {
                var success = function() {
                        var that = this,
                            args = arguments;

                        if (onSuccess) {
                            $rootScope.$apply(function() {
                                onSuccess.apply(that, args);
                            });
                        }
                    },
                    error = function() {
                        var that = this,
                            args = arguments;
                        if (onError) {
                            $rootScope.$apply(function() {
                                onError.apply(that, args);
                            });
                        }
                    };
                if (document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1 && document.URL.indexOf('file://') === -1) {
                    // PhoneGap application
                    cordovaReady(function(onSuccess, onError, options) {
                        navigator.geolocation.getCurrentPosition(success,
                            error, options);
                    });
                } else if (navigator.geolocation) {
                    // Web page
                    navigator.geolocation.getCurrentPosition(success,
                        error, options);
                }
            }
        };
    }
]);


/** API Service */
iescitiesAppServices
    .factory(
        'ApiService', [
            '$http',
            '$rootScope',
            '$log',
            function($http, $rootScope, $log) {
                return {


                    getIESCitiesApps: function(callback) {
                        $log.debug("ApiService.getIESCitiesApps()");
                        $.ajax({
                            //										url: "scripts/iescities/data/getIesCitiesApps.json",
                            url: "https://www.zaragoza.es/buscador/select?wt=json&q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:(%22Aplicaciones%22)&rows=50",
                            dataType: "json",

                            timeout: 15000,
                            async: true
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("------done---------");
                            callback(json);

                        }).error(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("------ error ---------");
                            callback(json);

                        }).fail(function(jqXHR, textStatus, errorThrown) {

                            if (jqXHR.status === 0) {

                               console.log("getIESCitiesApps - no se ha podido traer el listado de apps");
                                        

                            } else {

                                //alert('Uncaught Error: ' + jqXHR.responseText);
                                toastr.options.timeOut = 10000; 
                                toastr.error('No ha sido posible establecer la conexión. Inténtelo de nuevo mas tarde.');
                                toastr.options.timeOut = 2000; 
                                console.log("No ha sido posible establecer la conexión. Inténtelo de nuevo mas tarde.");

                            }

                        });




                        //								// GET: iesCities Apps
                        //								getIESCitiesApps : function(callback) {
                        //									$log.debug("ApiService.getIESCitiesApps()");									
                        //									// CORS 
                        //									//return $http.get('http://www.zaragoza.es/buscador/select?wt=json&q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:(%22Aplicaciones%22)').success(callback);
                        //									//return $http.get('scripts/iescities/data/getIesCitiesApps.json').success(callback);
                        //									return $http.get('http://www.zaragoza.es/buscador/select?wt=json&q=*:*%20AND%20-tipocontenido_s:estatico%20AND%20category:(%22Aplicaciones%22)').success(callback);
                    },

                    // POST: get avg rating of app
                    getAvgRating: function(appId, callback) {
                        $log.debug("ApiService.getAvgRatingByApp()");
                        $.ajax({
                            url: 'https://iescities.com/IESCities/api/data/query/253/sql',
                            type: "POST",
                            data: 'select avg(rating) as avgrating from comments where app = "' + appId + '"',
                            contentType: 'text/plain',
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback(json);

                        }).error(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback("error");

                        });


                    },


                    // POST: get avg rating of app
                    getAllRatings: function(callback) {
                        $log.debug("ApiService.getAllRatings()");
                        $.ajax({
                            url: 'https://iescities.com/IESCities/api/data/query/253/sql',
                            type: "POST",
                            data: 'select app, avg(rating) as avgrating from comments group by app',
                            contentType: 'text/plain',

                            timeout: 15000, // sets timeout to 4 seconds
                            async: true
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback(json);

                        }).error(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback("error");

                        }).fail(function(jqXHR, textStatus, errorThrown) {

                            if (jqXHR.status === 0) {

                               // alert('Not connect: Verify Network.');
                                toastr.options.timeOut = 10000; 
								toastr.error('No ha sido posible establecer la conexión. Compruebe su conexión y vuelva a intentarlo.');
								toastr.options.timeOut = 2000; 
								console.log("No ha sido posible establecer la conexión. Compruebe su conexión y vuelva a intentarlo.");
								        

                            } else {

                                console.log("getAllRatings - no se han podido traer los ratings");

                              /*  //alert('Uncaught Error: ' + jqXHR.responseText);
                                toastr.options.timeOut = 10000; 
								toastr.error('No ha sido posible establecer la conexión. Inténtelo de nuevo mas tarde.');
								toastr.options.timeOut = 2000; 
								console.log("No ha sido posible establecer la conexión. Inténtelo de nuevo mas tarde.");*/

                            }

                        });



                    },

                    // POST: get comments of app
                    getComments: function(appId, callback) {
                        $log.debug("ApiService.getCommentsByApp() " + appId);
                        $.ajax({
                            url: 'https://iescities.com/IESCities/api/data/query/253/sql',
                            type: "POST",
                            data: 'select * from comments where app = "' + appId + '"',
                            contentType: 'text/plain',
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback(json);

                        }).error(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback("error");

                        });


                    },
                    insertComment: function(comment, review, selectedApp, rating, date, callback) {
                        $log.debug("ApiService.insertComment()" + comment + review + selectedApp.id + " " + rating + " " + date);
                        $.ajax({
                            url: 'https://iescities.com/IESCities/api/data/update/253/sql',
                            type: "POST",
                            data: 'insert into comments(id,text,author,app,rating,date) values(NULL,"' + comment + '","' + review + '","' + selectedApp.id + '",' + rating + ',"' + date.toString('yyyy-MM-dd hh:mm:ss') + '")',
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_player:7QwtweHbPLOb"));
                            },
                            contentType: 'text/plain',
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback(json);
                        }).error(function(json) {
                            console.log(JSON.stringify(json));
                            console.log("---------------");
                            callback("error");

                        });

                    },
                    deleteComment: function(commentId) {
                        $log.debug("ApiService.deleteComment()" + comment + review + selectedApp.id + date);
                        $.ajax({
                            url: 'https://iescities.com/IESCities/api/data/update/250/sql',
                            type: "POST",
                            data: 'delete from comments where id=' + commentId,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_player:7QwtweHbPLOb"));
                            },
                            contentType: 'text/plain',
                        }).done(function(json) {
                            console.log(JSON.stringify(json));
                            return json;
                        });

                    }
                };
            }
        ]);

/** Log Service */
iescitiesAppServices
    .factory(
        'iesCitiesService', [
            '$http',
            '$rootScope',
            function($http, $rootScope) {
                return {

                    // Iescities server app log
                    sendLog: function(message, type) {
                        var uri = "https://iescities.com/IESCities/api/log/app/stamp/";
                        var timestamp = Math.floor(new Date()
                            .getTime() / 1000);
                        var appid = "zgzcollaborativemaps";
                        var session = $rootScope.sessionId;

                        uri = uri + timestamp + "/" + appid + "/" + session + "/" + type + "/" + message;
                        return $http({
                                method: 'GET',
                                url: uri,
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json;charset=UTF-8'
                                }
                            })
                            .success(
                                function(data) {
                                    console
                                        .log("iesCitiesService.sendLog() - SUCCESS");
                                    console
                                        .log(JSON
                                            .stringify(data));
                                })
                            .error(
                                function(data) {
                                    console
                                        .log("iesCitiesService.sendLog() - ERROR");
                                    console
                                        .log(JSON
                                            .stringify(data));
                                });
                    }

                };
            }
        ]);



/**
 * SESION DATA SERVICE
 *
 * Servicio para guardar informacion relativa a la sesion actual para controlar la navegacion.
 * Permite compartir informacion entre los controladores.
 *
 * */
iescitiesAppServices.factory('SesionData', ['$log', function($log) {

    $log.debug("SesionData.init()");

    var data = {
        selectedApp: null,
    };

    return {

        // User name
        getSelectedApp: function() {
            $log.debug("SesionData.getSelectedApp()");
            return data.selectedApp;
        },
        setSelectedApp: function(app) {
            $log.debug("SesionData.setSelectedApp()");
            data.selectedApp = app;
        },

        removeData: function() {
            $log.debug("SesionData.removeData()");
            data = {
                selectedApp: null
            };

        }

    };

}]);
